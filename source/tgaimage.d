module tgaimage;

public import tga;
import std.stdio;
import std.algorithm;

Pixel 	ubyteToPixel(ubyte r, ubyte g, ubyte b, ubyte a) {
	Pixel pix = Pixel();
	pix.r = r;
	pix.g = g;
	pix.b = b;
	pix.a = a;
	return pix;
}

class TGAImage {
protected:
	Pixel[] pixels;
	int width, height;
public:

	this() {
		this(0, 0);
	}

	this(int w, int h) {
		pixels = new Pixel[w * h];
		width = w;
		height = h;

		fill(pixels, ubyteToPixel(0, 0, 0, 255));
	}

	void write_tga_file(string filename) {
		Image img = createImage(pixels, cast(ushort)width, cast(ushort)height);

		File file = File(filename, "wb");
		writeImage(file, img);
	}

	bool set(int x, int y, Pixel pix) {
		if((pixels.length == 0) || (x < 0) || (y < 0) || (x >= width) || (y >= height)) {
			return false;
		}

		y = height - y - 1;

		pixels[y * width + x] = pix;

		return true;

	}

	Pixel get(int x, int y) {
		if (x<0 || y<0 || x>=width || y>=height) {
			return ubyteToPixel(0, 0, 0, 255);
		}
		return pixels[x+y*width];
	}
};