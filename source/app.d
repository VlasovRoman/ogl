import 	std.stdio;
import 	std.math;
import 	std.algorithm;
import 	tgaimage;
import 	model;
import 	geometry;
import 	std.conv;

immutable width = 800;
immutable height = 800;

const 	Pixel white = ubyteToPixel(255, 255, 255, 255);	
const 	Pixel red = ubyteToPixel(255, 0, 0, 255);
const 	Pixel green = ubyteToPixel(0, 255, 0, 255);

void 	line(ivec2 p1, ivec2 p2, TGAImage img, Pixel pix) {
	bool 	steep = false;
	if(abs(p1.x - p2.x) < abs(p1.y - p2.y)) {
		swap(p1.x, p1.y);
		swap(p2.x, p2.y);

		steep = true;
	}
	if(p1.x > p2.x) {
		swap(p1, p2);
	}

	int dx = p2.x - p1.x;
	int dy = p2.y - p1.y;

	float 	derror2 = abs(dy) * 2;
	float	error2 = 0;
	int 	y = p1.y;

	for (int x = p1.x; x <= p2.x; x++) {

		if(steep) 
			img.set(y, x, pix);
		else
			img.set(x, y, pix);

		error2 += derror2;

		if(error2 > dx) {
			y += (p2.y > p1.y ? 1 : -1);
			error2 -=  dx * 2;
		}
	}
}

void 	triangle(ivec2 p1, ivec2 p2, ivec2 p3, TGAImage img, Pixel pix) {
	if((p1.y == p2.y) && (p2.y == p3.y))
		return;

	if(p1.y > p2.y)
		swap(p1, p2);
	if(p1.y > p3.y)
		swap(p1, p3);
	if(p2.y > p3.y)
		swap(p2, p3);

	int total_height = p3.y - p1.y;

	foreach(y; 0..total_height) {
		bool 	second_half = y > (p2.y - p1.y) || (p2.y == p1.y);

		int		segment_height = second_half ? (p3.y - p2.y) : (p2.y - p1.y);

		float 	alpha 	= to!(float)(y) / total_height;
		float 	beta 	= to!(float)(y - (second_half ? (p2.y - p1.y) : 0)) / segment_height;

		ivec2	a = p1 + iVecToFloat((p3 - p1), alpha);
		ivec2	b = (second_half ? (p2 + iVecToFloat((p3 - p2), beta)) : (p1 + iVecToFloat((p2 - p1), beta)));

		if(a.x > b.x)
			swap(a, b);

		foreach(x; (a.x)..(b.x + 1))
			img.set(x, p1.y + y, pix);
	}
}

void main() {
	Model model = new Model("./obj/african_head.obj");

	TGAImage 	img = new TGAImage(width, height);
	vec3 	light_dir = vec3(0, 0, -1);

	foreach(i; 0..model.nfaces()) {
		ivec3 	face = model.face(i);
		ivec2 	screen_coords[3];
		vec3	world_coords[3];

		for (int j=0; j<3; j++) {
			vec3 	vec = model.vert(face[j]);

			int x = cast(int)((vec.x+1.)*width/2.);
			int y = cast(int)((vec.y+1.)*height/2.);

			screen_coords[j] = ivec2(x, y);
			world_coords[j] = vec;
		}

		//vec3 n = (world_coords[2] - world_coords[0]) * (world_coords[1] - world_coords[0]);

		vec3 n = what(world_coords[2] - world_coords[0], world_coords[1] - world_coords[0]);
		n = n.e();

		float intensity = getVecForVec(n, light_dir);

		if(intensity > 0)
			triangle(screen_coords[0], screen_coords[1], screen_coords[2], img, ubyteToPixel(to!(ubyte)(intensity * 255), to!(ubyte)(intensity * 255), to!(ubyte)(intensity * 255), 255));
	}
	img.write_tga_file("output.tga");
}

