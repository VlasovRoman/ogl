module geometry;

import 	std.conv;

public import des.math;

ivec2 	iVecToFloat(ivec2 vec, float x) {
	foreach(i; 0..2)
		vec[i] = to!(int)(to!(float)(vec[i] * x));

	return vec;
}

float 	getVecForVec(vec3 v1, vec3 v2) {
	return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
}

vec3 what(vec3 v1, vec3 v2) {
	return vec3(v1[1]*v2[2] - v1[2]*v2[1], v1[2]*v2[0] - v1[0]*v2[2], v1[0]*v2[1] - v1[1]*v2[0]);
}