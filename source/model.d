module model;

import des.math;

import std.stdio;
import std.array;
import std.string;
import std.conv;

class Model {
	this(string 	filename) {
		File 	file = File(filename);

		while (!file.eof()) {
			string 	line = file.readln();

			string[] 	parsed = array(splitter(line));	

			if(parsed.length == 0) {
				continue;
			}

			if(parsed[0] == "v") {
				vec3 vec = vec3();

				foreach(i; 0..3)
					vec[i] = to!(double)(parsed[i + 1]);

				verts_ ~= vec;
			}
			else if(parsed[0] == "f") {
				ivec3 vec = ivec3();

				foreach(i; 0..3) {
					string[3] info = array(splitter(parsed[i + 1], "/"));

					vec[i] = to!(int)(info[0]) - 1;
				}

				faces_ ~= vec;
			}
		}
	}

	//~this() {}

	int 		nverts() {
		return cast(int)verts_.length;
	}

	int 		nfaces() {
		return cast(int)faces_.length;
	}

	vec3		vert(int i) {
		return verts_[i];
	}

	ivec3 		face(int idx) {
		return faces_[idx];
	}
protected:
	
	vec3[]		verts_;
	ivec3[] 	faces_;
}